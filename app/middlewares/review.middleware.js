const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get all review Middleware");

    next();
}

const createReviewMiddleware = (req, res, next) => {
    console.log("Create review Middleware");

    next();
}

const getDetailReviewMiddleware = (req, res, next) => {
    console.log("Get detail review Middleware");

    next();
}

const updateReviewMiddleware = (req, res, next) => {
    console.log("Update review Middleware");

    next();
}

const deleteReviewMiddleware = (req, res, next) => {
    console.log("Delete review Middleware");

    next();
}

module.exports = {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getDetailReviewMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}